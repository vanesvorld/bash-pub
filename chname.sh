#!/bin/bash
#
# chname.sh v0.0.2-CVE
# Script to change Centos hostname
#

clear

hn=`hostnamectl status | grep host`
echo "Current" $hn ; sleep 1 ; echo ; echo "Enter desired hostname, then press RETURN <cr> : "; read hn

hnn='HOSTNAME=centos-'$hn
hnp='/etc/sysconfig/network'

echo "Setting centos-" $hn ; echo ; echo "Really do it?" 
echo "(<cr> to ACCEPT)" ; echo "(CTRL+C to ABORT)"; read whatever

# NOP existing statments
sudo sed -i 's/^H/#H/' $hnp

# Set new values
echo $hnn | sudo tee -a $hnp 2>&1 >/dev/null
hostnamectl set-hostname "centos-"$hn

#cat $hnp ; echo ; echo "You may want to change the stuff in hosts file too:"
#127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
#::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
#cat /etc/hosts ; hostnamectl status | grep host

echo "You may want to restart networking, and then the system."
echo; sleep 1 ; echo "Goodbye." ; sleep .5 ; exit