#!/bin/bash
#
# zt_bs.sh 
# bootstrap zt installation v0.0.2-cve
#
# be root
#

cd ~/

if [`hostnamectl| grep -c arm`]
then 
  apt update
  curl -s https://install.zerotier.com | sudo bash
else
  yum update
  yum install -y gcc gcc-c++ make epel-release git
  yum install -y centos-release-scl && yum install -y devtoolset-8-gcc devtoolset-8-gcc-c++
  git clone https://github.com/zerotier/ZeroTierOne.git
  cd ZeroTierOne
  make
  cp debian/zerotier-one.service /etc/systemd/system/ && chmod 755 /etc/systemd/system/zerotier-one.service
  cp zerotier-cli /usr/sbin/zerotier-cli
  cp zerotier-one /usr/sbin/zerotier-one
  systemctl enable zerotier-one.service && systemctl start zerotier-one.service
fi

zerotier-cli info
