#!/bin/bash
#
# homebrew_bs.sh
# bootstrap homebrew installation v0.0.1-cve
#
# be root
#
echo "Installing homebrew";
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"

# C++ dev env
brew tap ArmMbed/homebrew-formulae
brew tap osx-cross/homebrew-arm

brew install arm-none-eabi-gcc
brew install gdb
